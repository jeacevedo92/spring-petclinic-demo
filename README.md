# Spring PetClinic Sample Application

Spring Petclinic is a sample Spring Boot application demonstrating the capabilities of the Spring framework. It's a web-based application for managing a pet clinic, including functionalities like managing owners, pets, and veterinarians.
![img.png](images%2Fimg.png)


## CI-CD diagram
![cicd_petclinic.drawio.png](images%2Fcicd_petclinic.drawio.png)

### Prerequisites
The following items should be installed in your system:
* Java 17 or newer (full JDK, not a JRE).
* [git command line tool](https://help.github.com/articles/set-up-git)
* Your preferred IDE

## setting environment variables

For the execution of the application you must take into account the variables for database

DB_HOST
DB_PASS
DB_USER

## Running petclinic locally
Petclinic is a [Spring Boot](https://spring.io/guides/gs/spring-boot) application built using [Maven](https://spring.io/guides/gs/maven/) or [Gradle](https://spring.io/guides/gs/gradle/). You can build a jar file and run it from the command line (it should work just as well with Java 17 or newer):

```
git clone https://github.com/spring-projects/spring-petclinic.git
cd spring-petclinic
```

you can also use the provided `docker-compose.yml` file to start the database containers. Each one has a profile just like the Spring profile:

```
$ docker-compose --profile mysql up
```

or

```
$ docker-compose --profile postgres up
```

### To run with the database embedded by default

```
./mvnw clean package
java -jar target/*.jar
```

### To run with spring boot profile

```
./mvnw clean package -Dspring-boot.run.profiles=mysqql
java -jar target/*.jar --spring.profiles.active=mysql
```

You can then access petclinic at http://localhost:8080/

Or you can run it from Maven directly using the Spring Boot Maven plugin. If you do this, it will pick up changes that you make in the project immediately (changes to Java source files require a compile as well - most people use an IDE for this):

```
./mvnw spring-boot:run
mvn spring-boot:run -Dspring-boot.run.profiles=mysql

```

## ARQUITECTURE

![arquitectura_petclinic.drawio (4).png](images%2Farquitectura_petclinic.drawio%20%284%29.png)
