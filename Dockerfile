FROM openjdk:17
ADD target/*.jar ecs.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/ecs.jar","--spring.profiles.active=mysql"]
